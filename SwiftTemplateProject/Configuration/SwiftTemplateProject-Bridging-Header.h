//
//  SwiftTemplateProject-Bridging-Header.h
//  SwiftTemplateProject
//
//  Created by Pham Hoa on 8/7/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

#ifndef SwiftTemplateProject_Bridging_Header_h
#define SwiftTemplateProject_Bridging_Header_h

@import EVReflection;
@import RxSwift;
@import RxCocoa;
@import NVActivityIndicatorView;
@import UIColor_Hex_Swift;
@import AFDateHelper;
@import XLPagerTabStrip;
@import RealmSwift;
@import SwiftyJSON;

#endif /* SwiftTemplateProject_Bridging_Header_h */
