//
//  Constants.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation

struct Constants {
    
    // MARK: - Application
    struct App {
        
        // MARK: Main
        static let isHTTPS = false
        
        // MARK: Base

        #if DEVELOPMENT // -> Development
        
        static let baseAPIURL = "http://biologic.ap-southeast-1.elasticbeanstalk.com"
        
        #else // -> Production
        
        static let baseAPIURL = "http://staging.bio-logic.xyz"
        
        #endif
        
        static let navigationBarColor = "0070BE"
        
        // MARK: DateTime
        
        static let kOnlyDateFormat = "mm/dd/yyyy"
        static let kOnlyTimeFormat = "hh:MM a"
    }
    
    struct Segue {
        static let kLEFTMENU_TO_HOME = "LEFTMENU_TO_HOME"
        static let kSEGUE_LOGIN_TO_MAIN = "SEGUE_LOGIN_TO_MAIN"
    }
    
    struct GeneralNumberFormat {
        // MARK: General number formatter keys
        
        /// General ordinal key
        static let NumberFormatterOrdinalKey = "ordinal"
        /// General spell out key
        static let NumberFormatterSpellOutKey = "spellout"
        /// General distance key
        static let NumberFormatterDistanceKey = "distance"
        
        // MARK: Mass number formatter keys
        
        /// Mass generic key
        static let MassFormatterGenericKey = "mass_generic"
        /// Mass person key
        static let MassFormatterPersonKey = "mass_person"
    }
    
    struct StoryboardName {
        static let foodDiary = "Food"
        static let progress  = "Progress"
        static let feed      = "Feed"
        static let learn     = "Learn"
        static let settings  = "Setting"
        static let main      = "Main"
    }
}
