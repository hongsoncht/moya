//
//  LoginViewController.swift
//  SwiftTemplateProject
//
//  Created by Pham Hoa on 8/7/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: IBOutlet
    
    @IBOutlet weak var txfUsername: UITextField!
    @IBOutlet weak var txfPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    // MARK: Property
    
    var viewModel: LoginViewModel!
    let disposeBag = DisposeBag()
    
    // MARK: LifeCycle
    
    deinit {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configViewController(.root, leftType: .none, rightType: .none)
        self.viewModel = LoginViewModel()
        self.bindingData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNaivigationTitle(.titleOnly(title: "Login"))
    }
    
    // MARK: IBAction
    
    // MARK: Public
    
    // MARK: Private
    
    private func bindingData() {
        viewModel.email.bind(to: txfUsername.rx.text.orEmpty).disposed(by: disposeBag)
        txfUsername.rx.text.orEmpty.bind(to: viewModel.email).disposed(by: disposeBag)
        //
        viewModel.password.bind(to: txfPassword.rx.text.orEmpty).disposed(by: disposeBag)
        txfPassword.rx.text.orEmpty.bind(to: viewModel.password).disposed(by: disposeBag)

        //
        let isValidToLogin = Observable.combineLatest(txfUsername.rx.text.orEmpty.asObservable(), txfPassword.rx.text.orEmpty.asObservable()) {
            return $0.isValidEmail() == true && $1.isValidPassword() == true
        }
        isValidToLogin.bind(to: btnLogin.rx.isEnabled).disposed(by: disposeBag)
        
        // actions
        
        btnLogin.rx.tap.asObservable().subscribe(onNext: { [weak self] in
            self?.view.endEditing(true)
            self?.viewModel.submitLogin()
        }).disposed(by: disposeBag)
        
        // presenter
        
        viewModel.isLoading.asObservable().subscribe(onNext: { (isLoading) in
            isLoading ? Utils.showLoadingIndicator() : Utils.hideLoadingIndicator()
        }).disposed(by: disposeBag)
        viewModel.loginSuccessed.asObservable().subscribe(onNext: { [weak self] (isLogged) in
            if isLogged {
                self?.performSegue(withIdentifier: Constants.Segue.kSEGUE_LOGIN_TO_MAIN, sender: nil)
            }
        }).disposed(by: disposeBag)
        viewModel.error.skip(1).subscribe(onNext: { [weak self] (message) in
            self?.showToast(message: message)
        }).disposed(by: disposeBag)
    }
}
