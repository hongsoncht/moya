//
//  LoginViewModel.swift
//  SwiftTemplateProject
//
//  Created by Pham Hoa on 8/7/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire

struct LoginViewModel {
    var email = BehaviorRelay<String>(value: "hoa.pham@vmodev.com")
    var password = BehaviorRelay<String>(value: "123456")
    var isLoading = BehaviorSubject<Bool>(value: false)
    var loginSuccessed = BehaviorSubject<Bool>(value: false)
    var error = BehaviorSubject<String>(value: "")
    
    init() {
        //
    }
    
    func submitLogin() {
        self.loginSuccessed.on(.next(true))
        return
        
        // demo
        
        if !validate() {
            return
        }

        let email = self.email.value
        let password = self.password.value
        
        firstly { () -> Promise<ApiResponse> in
            self.isLoading.on(.next(true))
            return ApiClient.shared.callApi(AuthApi.login(username: email, password: password, deviceToken: ApplicationManager.shared.getAppConfig().deviceToken))
            }.done({ (response) in
                if let data = response.data {
                    Session.shared.saveUserProfile(user: UserProfile.init(json: data))
                }
            }).then({ () -> Promise<ApiResponse> in
                return ApiClient.shared.callApi(UserApi.getInfo)
            }).done({ (response) in
                if let data = response.data {
                    Session.shared.saveUserProfile(user: UserProfile.init(json: data))
                }
                self.loginSuccessed.on(.next(true))
            }).ensure(on: DispatchQueue.main) {
                self.isLoading.on(.next(false))
            }.catch { (err) in
                self.error.onNext(err.localizedDescription)
        }
    }
    
    func validate() -> Bool {
        let email = self.email.value
        let password = self.password.value
        
        if email.isEmpty {
            error.onNext("login.email.isEmpty".localized())
            return false
        }
        
        if !email.isValidEmail() {
            error.onNext("login.email.invalid".localized())
            return false
        }
        
        if password.isEmpty {
            error.onNext("login.password.isEmpty".localized())
            return false
        }
        
        if !password.isValidPassword() {
            error.onNext("login.password.invalid".localized())
            return false
        }
        
        return true
    }
}
