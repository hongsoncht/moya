//
//  LeftMenuViewModel.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit

enum LeftMenuItem {
    case home
    case document
    case signature
    case logout
    case none
}

class LeftMenuViewModel: NSObject {
    var dataSource: [LeftMenuItem] = [.home, .document, .signature, .logout]
}

