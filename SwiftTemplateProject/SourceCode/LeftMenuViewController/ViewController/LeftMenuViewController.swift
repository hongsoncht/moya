//
//  LeftMenu.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit
import SideMenu

protocol LeftMenuDelegate {
    func selectedIndexPath(indexPath: IndexPath)
}

class LeftMenuViewController : UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableViewLeftMenu: UITableView!
    
    // MARK: - Define
    
    // MARK: - Variables
    var viewModel: LeftMenuViewModel? = LeftMenuViewModel()
    var delegate: LeftMenuDelegate?
    
    // MARK: - LifeCycle
    
    deinit {
        viewModel = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initCommon()
        self.configViewController(.none, leftType: .none, rightType: .none)
        self.setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
   
    func setupTableView() {
        self.tableViewLeftMenu.registerCell(AvatarLeftMenuTableViewCell.self)
        self.tableViewLeftMenu.registerCell(LeftMenuItemTableViewCell.self)
        self.tableViewLeftMenu.dataSource = self
        self.tableViewLeftMenu.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
}

extension LeftMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            // Avatar
            return 1
        }
        
        return self.viewModel?.dataSource.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Avatar section
        if indexPath.section == 0 {
            return tableView.dequeueReusableCell(withIdentifier: AvatarLeftMenuTableViewCell.identifierView, for: indexPath)
        }
        
        return tableView.dequeueReusableCell(withIdentifier: LeftMenuItemTableViewCell.identifierView, for: indexPath)
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 150
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        if let cell = cell as? LeftMenuItemTableViewCell {
            let itemType = self.viewModel?.dataSource[indexPath.row] ?? .none
            switch itemType {
            case .home:
                cell.imgItem.image = #imageLiteral(resourceName: "ic_cancel")
                cell.titleItem.text = "Trang chủ"
            case .document:
                cell.imgItem.image = #imageLiteral(resourceName: "ic_cancel")
                cell.titleItem.text = "Tài Liệu"
            case .signature:
                cell.imgItem.image = #imageLiteral(resourceName: "ic_cancel")
                cell.titleItem.text = "Chữ ký"
            case .logout:
                cell.imgItem.image = #imageLiteral(resourceName: "ic_cancel")
                cell.titleItem.text = "Thoát"
            default:
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let itemType = self.viewModel?.dataSource[indexPath.row] ?? .none
        switch itemType {
        case .home:
            self.performSegue(withIdentifier: Constants.Segue.kLEFTMENU_TO_HOME, sender: nil)
        default:
            break
        }
    }
}
