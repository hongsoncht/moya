//
//  MainViewController.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import SideMenu
import PromiseKit

class MainViewController: UIViewController {
    
    // MARK: IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Variable
    var viewModel: MainViewModel? = MainViewModel()
    let bag = DisposeBag()
    
    // MARK: Life Cycle
    deinit {
        viewModel = nil
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSideMenu()
        self.configViewController(.root, leftType: .menu, rightType: .none)
        self.configTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNaivigationTitle(.titleOnly(title: "Home"))
        self.setupNavigationBar(color: UIColor.red.withAlphaComponent(0))
    }
    
    func configTableView() {
        self.tableView.rowHeight = 44
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rx.contentOffset.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { (point) in
            var alpha: CGFloat = 0
            alpha = point.y / 200
            let color = UIColor.red.withAlphaComponent(alpha)
            self.setupNavigationBar(color: color)
        }).disposed(by: bag)
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    // MARK: IBAction
}

extension MainViewController {
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view, forMenu: .left)
        
        SideMenuManager.default.menuPushStyle = .popWhenPossible
        SideMenuManager.default.menuAllowPushOfSameClassTwice = false
        SideMenuManager.default.menuFadeStatusBar = false
    }
}

extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = "\(indexPath.row)"
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    
}
