//
//  RealmManager.swift
//  Aimee
//
//  Created by Pham Hoa on 1/20/18.
//  Copyright © 2018 Pham Hoa. All rights reserved.
//

import UIKit
import RealmSwift

let kRealmResource = ""

class RealmManager: NSObject {
    
    static let shared = RealmManager()
    
    override private init() {
        //
    }
    
    ///< preload
    func preload() {
        var defaultRealmConfig = Realm.Configuration.defaultConfiguration

        // Migration
        defaultRealmConfig.schemaVersion = 0
        defaultRealmConfig.migrationBlock = {(migration, oldVersion) in
            // migration code here
        }
        Realm.Configuration.defaultConfiguration = defaultRealmConfig
    }
    
    func clearRealmDB() {
        RealmManager.shared.write { (realm) in
            // delete all objects
            realm.deleteAll()
            
            // remove all files by path
            let realmURL = Realm.Configuration.defaultConfiguration.fileURL!
            let realmURLs = [
                realmURL,
                realmURL.appendingPathExtension("lock"),
                realmURL.appendingPathExtension("note"),
                realmURL.appendingPathExtension("management")
            ]
            for url in realmURLs {
                do {
                    try FileManager.default.removeItem(at: url)
                    print("clear realm database: \(url.lastPathComponent) successfully!")
                } catch {
                    print("clear realm database: \(url.lastPathComponent) failed!")
                }
            }
        }
    }

    ///< write2, an improving of (write) function
    func write(inTransaction: @escaping ((Realm) -> Void)) {
        autoreleasepool {
            //-- init realm
            do {
                let realm = try Realm()
                
                //-- begin writting
                realm.beginWrite()
                
                //-- call transaction: part code to update objects
                inTransaction(realm)
                
                //-- end writting
                try? realm.commitWrite()
                
                //-- refresh realm to get last updated objects
                realm.refresh()
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
}
