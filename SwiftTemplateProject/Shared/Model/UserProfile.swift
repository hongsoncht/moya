//
//  UserProfile.swift
//  HATConsumer
//
//  Created by Pham Hoa on 8/10/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserProfile: EVObject, NSCopying {
    var id: NSNumber?
    var password: String?
    var fullname: String?
    var avatar: String?
    var email: String?
    var token: String?

    public init(id: NSNumber?, password: String?, fullname: String?, avatar: String?, email: String?, token: String?) {
        self.id = id
        self.password = password
        self.fullname = fullname
        self.avatar = avatar
        self.email = email
        self.token = token
    }

    init(json: JSON) {
        self.id                  = json["id"].number
        self.fullname            = json["name"].string
        self.password            = json["password"].string
        self.avatar              = json["avatar"].string
        self.email               = json["email"].string
        self.token               = json["token"].string
    }
    
    required init() {
        super.init()
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = UserProfile.init(id: id, password: password, fullname: fullname, avatar: avatar, email: email, token: token)
        return copy
    }
    
    // MARK: - Method:
}
