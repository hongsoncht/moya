//
//  ApiResponse.swift
//  SwiftTemplateProject
//
//  Created by Pham Hoa on 7/2/18.
//  Copyright © 2018 Pham Hoa. All rights reserved.
//

import Foundation
import SwiftyJSON
import Moya

public class ApiResponse {
    
    let error: NSError?
    let data: JSON?
    
    init(response: Response) {
        if response.isUnauthenticated {
            let json = JSON(response.data)
            self.error = NSError(
                domain: ServiceHelper.errorServiceDomain,
                code: ServiceHelper.errorCodeUnauthenticated,
                userInfo: [NSLocalizedDescriptionKey: json["message"].stringValue])
            self.data = nil
        } else if response.isUnauthorized {
            let json = JSON(response.data)
            self.error = NSError(
                domain: ServiceHelper.errorServiceDomain,
                code: ServiceHelper.errorCodeUnauthorized,
                userInfo: [NSLocalizedDescriptionKey: json["message"].stringValue])
            self.data = nil
            Session.shared.logout(self.error?.localizedDescription)
        } else if response.isGatewayTimeOut {
            self.error = NSError(
                domain: ServiceHelper.errorServiceDomain,
                code: ServiceHelper.errorCodeGatewayTimeOut,
                userInfo: [NSLocalizedDescriptionKey: "network.error.request.timed.out".localized()])
            self.data = nil
        } else if response.isSuccess {
            let json = JSON(response.data)
            if json["status"].exists() && json["status"].intValue == 1 {
                 self.error = nil
            } else {
                self.error = NSError(
                    domain: ServiceHelper.errorServiceDomain,
                    code: response.statusCode,
                    userInfo: [NSLocalizedDescriptionKey: json["message"].stringValue])
            }
            if json["data"].exists() {
                self.data = json["data"]
            } else {
                self.data = json
            }
        } else {
            self.error = NSError(
                domain: ServiceHelper.errorServiceDomain,
                code: response.statusCode,
                userInfo: [NSLocalizedDescriptionKey: "Something went wrong!"])
            self.data = nil
        }
    }
    
    init(error: Error) {
        let nsError = error as NSError
        
        self.error = NSError(
            domain: nsError.domain,
            code: nsError.code,
            userInfo: [NSLocalizedDescriptionKey: nsError.localizedDescription])
        self.data = nil
    }
}
