//
//  AuthApi.swift
//  SwiftTemplateProject
//
//  Created by Pham Hoa on 6/29/18.
//  Copyright © 2018 Pham Hoa. All rights reserved.
//

import Foundation
import Moya

public enum AuthApi {
    case login(username: String, password: String, deviceToken: String?)
    case logout
}

extension AuthApi: TargetType {
    public var baseURL: URL {
        return URL(string: Constants.App.baseAPIURL)!
    }
    
    public var path: String {
        switch self {
        case .login(_):
            return "/api/auth/login"
        case .logout:
            return "/api/logout"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .login(_):
            return .post
        case .logout:
            return .post
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .login(let username, let password, let deviceToken):
            let params = [
                "username": username,
                "password": password,
                "osType": "ios",
                "deviceToken": deviceToken ?? "abc"
            ]
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        case .logout:
            return .requestPlain
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        case .login(_):
            return [
                "Content-Type": "application/json",
                "X-Requested-With": "XMLHttpRequest"
            ]
        case .logout:
            return [
                "Authorization": "Bearer \(Session.shared.getCurrentUser()?.token ?? "")"
            ]
        }
    }
}
