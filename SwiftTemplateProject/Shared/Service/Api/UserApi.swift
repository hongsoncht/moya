//
//  UserApi.swift
//  SwiftTemplateProject
//
//  Created by Pham Hoa on 7/2/18.
//  Copyright © 2018 Pham Hoa. All rights reserved.
//

import Foundation
import Moya

public enum UserApi {
    case getInfo
}

extension UserApi: TargetType {
    public var baseURL: URL {
        return URL(string: Constants.App.baseAPIURL)!
    }
    
    public var path: String {
        switch self {
        case .getInfo:
            return "/api/user/profile"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .getInfo:
            return .post
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getInfo:
            return .requestPlain
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        case .getInfo:
            return nil
        }
    }
}

