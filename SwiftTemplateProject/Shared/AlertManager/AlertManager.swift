//
//  AlertManager.swift
//  HATConsumer
//
//  Created by Pham Hoa on 8/16/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation

class AlertManager {
    
    // MARK: - Class Properties
    
    static let sharedInstance = AlertManager()
    
    // MARK: - Instance Properties
    
    // MARK: - Init/Deinit
    
    private init() {
        //
    }
    
    private func present(_ alert: UIAlertController) {
        if let topVc = Utils.topViewController() {
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                alert.popoverPresentationController?.sourceView = topVc.view
                alert.popoverPresentationController?.sourceRect = CGRect.init(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: 0)
                alert.popoverPresentationController?.permittedArrowDirections = .down
                DispatchQueue.main.async {
                    topVc.present(alert, animated: true, completion: nil)
                }
            } else {
                DispatchQueue.main.async {
                    topVc.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - Instance Methods
    
    func showAlertDefault(_ title: String?, message: String?, buttons: [String], completed:((_ index: Int) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        if buttons.count == 1 {
            alert.addAction(UIAlertAction(title: buttons[0], style: .default, handler: { _ in
                alert.dismiss(animated: true, completion: nil)
                completed?(0)
            }))
        } else if buttons.count > 1 {
            for (index, title) in buttons.enumerated() {
                alert.addAction(UIAlertAction(title: title, style: .default, handler: { _ in
                    alert.dismiss(animated: true, completion: nil)
                    completed?(index)
                }))
            }
        }
        
        self.present(alert)
    }

    func showInputAlert(_ title: String?, message: String?, placeHolders: [String?], buttons: [String], config: ((UIAlertController) -> Void)?, completed:((_ index: Int, _ inputs: [String?]) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        for input in placeHolders {
            alertController.addTextField { (textField: UITextField!) -> Void in
                textField.placeholder = input
            }
        }
        
        if buttons.count == 1 {
            alertController.addAction(UIAlertAction(title: buttons[0], style: .default, handler: { _ in
                alertController.dismiss(animated: true, completion: nil)
                completed?(0, alertController.textFields?.map({ return $0.text }) ?? [])
            }))
        } else if buttons.count > 1 {
            for (index, title) in buttons.enumerated() {
                alertController.addAction(UIAlertAction(title: title, style: .default, handler: { _ in
                    alertController.dismiss(animated: true, completion: nil)
                    completed?(index, alertController.textFields?.map({ return $0.text }) ?? [])
                }))
            }
        }
        config?(alertController)
        self.present(alertController)
    }
}
