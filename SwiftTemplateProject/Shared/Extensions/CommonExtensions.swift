//
//  UIColor+Extension.swift
//  Vsign
//
//  Created by Pham Hoa on 3/21/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hexString: String) {
        let hexStr = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner            = Scanner(string: hexStr)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
    
    convenience init(rgb: (r: CGFloat, g: CGFloat, b: CGFloat)) {
        self.init(red: rgb.r/255, green: rgb.g/255, blue: rgb.b/255, alpha: 1.0)
    }

    func toHexString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb: Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format: "#%06x", rgb)
    }
}

/* Extention SeggmentControll*/
extension UISegmentedControl {
    func makeMultiline(withFontName font: UIFont, textColor: UIColor) {
        for index in 0...self.numberOfSegments - 1 {
            
            let label = UILabel(frame: CGRect.init(x: 0, y: 0, width: Double(self.frame.width)/Double(self.numberOfSegments), height: Double(self.frame.height)))
            label.font = font
            label.textColor = textColor
            label.text = self.titleForSegment(at: index)
            label.numberOfLines = 0
            label.textAlignment = .center
            label.adjustsFontSizeToFitWidth = false
            
            self.setTitle("", forSegmentAt: index)
            self.subviews[index].addSubview(label)
        }
    }
    func removeBorders() {
        setBackgroundImage(imageWithColor(color: BLColor.Background.screen), for: .normal, barMetrics: .default)
        setBackgroundImage(imageWithColor(color: BLColor.Background.screen), for: .selected, barMetrics: .default)
        setDividerImage(imageWithColor(color: BLColor.Background.screen), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    
    func removeBorders(selectedColor: UIColor, normalColor: UIColor) {
        setBackgroundImage(imageWithColor(color: normalColor), for: .normal, barMetrics: .default)
        setBackgroundImage(imageWithColor(color: selectedColor), for: .selected, barMetrics: .default)
        setDividerImage(imageWithColor(color: normalColor), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    
    // create a 1x1 image with this color
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension UISearchBar {
    func change(font: UIFont?, _ color: UIColor = BLColor.Background.screen) {
        for view in (self.subviews[0]).subviews {
            if let textFiled = view as? UITextField {
                textFiled.font = font
                textFiled.backgroundColor = color
                textFiled.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: BLColor.Text.secondary])
            }
            self.setImage(UIImage(), for: .search, state: .normal)
        }
    }
}

extension Array {
    func convertToList<Int>() -> List<Int> {
        let res = List<Int>()
        for item in self {
            res.append(item as! Int)
        }
        return res
    }
}
