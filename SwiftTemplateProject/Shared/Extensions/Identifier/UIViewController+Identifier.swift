//
//  NSViewController+Identifier.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation
import UIKit

//
// MARK: - Default Implementation for Identifier
extension UIViewController: Identifier {
    
    /// ID View
    static var identifierView: String {
        get {
            return String(describing: self)
        }
    }
    
    /// XIB
    static func xib() -> UINib? {
        return UINib(nibName: self.identifierView, bundle: nil)
    }
}
