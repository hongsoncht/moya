//
//  String+Extension.swift
//  Aimee
//
//  Created by user on 10/16/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit

extension String {
    // Common
    
    func matches(for regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPhone() -> Bool {
        let type: NSTextCheckingResult.CheckingType = .phoneNumber
        
        if let detector = try? NSDataDetector.init(types: type.rawValue) {
            let matchs = detector.matches(in: self, options: .anchored, range: NSMakeRange(0, self.count))
            if matchs.count == 1 {
                return true
            }
        }
        return false
    }
    
    func isValidPassword() -> Bool {
        if self.contains(where: { $0 == " " }) {
            return false
        }
        if self.count < 6 {
            return false
        }
        
        /*
        if self.matches(for: "[a-z]+").count == 0 {
            return false
        }
        
        // Check decimal
        if self.matches(for: "\\d+").count == 0 {
            return false
        }
        */
        
        return true
    }
    
//    var isEmail: Bool {
//        get {
//            do {
//                let express = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
//                return express.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
//            } catch {
//                return false
//            }
//        }
//
//    }
    
//    var isPassword: Bool {
//        get {
//            if self.characters.count < 6 {
//                return false
//            }
////            
////            // Check upercase char
////            if self.matches(for: "[A-Z]+").count == 0 {
////                return false
////            }
//            
//            // Check lowercase char
//            if self.matches(for: "[a-z]+").count == 0 {
//                return false
//            }
//            
//            // Check decimal
//            if self.matches(for: "\\d+").count == 0 {
//                return false
//            }
//            
//            return true
//        }
//    }

    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
