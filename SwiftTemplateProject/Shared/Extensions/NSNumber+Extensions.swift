//
//  NSNumber+Extensions.swift
//  Vsign
//
//  Created by Pham Hoa on 3/21/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation

extension NSNumber {
    
    func getDateString(format: String? = Constants.App.kOnlyDateFormat) -> String? {
        let date = Date.init(timeIntervalSince1970: self.doubleValue)
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.local
        formatter.locale = Locale.current
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    func timeAgoSinceDate(_ numericDates: Bool) -> String {
        let date = Date.init(timeIntervalSince1970: self.doubleValue)
        let components = Calendar.current.dateComponents(Set<Calendar.Component>([.minute, .hour, .day, .weekOfYear, .month, .year, .second]), from: date, to: Date())
        
        if let year = (components.year), year >= 2 {
            return "\(components.year!) years ago"
        } else if let year = (components.year), year >= 1 {
            if numericDates {
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if let month = (components.month), month >= 2 {
            return "\(components.month!) months ago"
        } else if let month = (components.month), month >= 1 {
            if numericDates {
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if let weekOfYear = (components.weekOfYear), weekOfYear >= 2 {
            return "\(components.weekOfYear!) weeks ago"
        } else if let weekOfYear = (components.weekOfYear), weekOfYear >= 1 {
            if numericDates {
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if let day = (components.day), day >= 2 {
            return "\(components.day!) days ago"
        } else if let day = (components.day), day >= 1 {
            if numericDates {
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if let hour = (components.hour), hour >= 2 {
            return "\(components.hour!) hours ago"
        } else if let hour = (components.hour), hour >= 1 {
            if numericDates {
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if let minute = (components.minute), minute >= 2 {
            return "\(components.minute!) minutes ago"
        } else if let minute = (components.minute), minute >= 1 {
            if numericDates {
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if let second = (components.second), second >= 3 {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
}

extension Double {
    
    func splitIntoParts(decimalPlaces: Int, round: Bool) -> (leftPart: Int, rightPart: Int) {
        
        var number = self
        if round {
            //round to specified number of decimal places:
            let divisor = pow(10.0, Double(decimalPlaces))
            number = Darwin.round(self * divisor) / divisor
        }
        
        //convert to string and split on decimal point:
        let parts = String(number).components(separatedBy: ".")
        
        //extract left and right parts:
        let leftPart = Int(parts[0]) ?? 0
        let rightPart = Int(parts[1]) ?? 0
        
        return(leftPart, rightPart)
        
    }
    
    func splitFootsIntoParts(decimalPlaces: Int, round: Bool) -> (leftPart: Int, rightPart: Int) {
        
        var number = self
        if round {
            //round to specified number of decimal places:
            let divisor = pow(100.0, Double(decimalPlaces))
            number = Darwin.round(self * divisor) / divisor
        }
        
        //convert to string and split on decimal point:
        let parts = String(number).components(separatedBy: ".")
        
        //extract left and right parts:
        let leftPart = Int(parts[0]) ?? 0
        let rightPart = Int(parts[1]) ?? 0
        
        return(leftPart, rightPart)
        
    }
}
