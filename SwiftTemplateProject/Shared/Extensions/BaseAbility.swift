//
//  BaseAbility.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation

protocol BaseAbility: class {
    func initCommon()
}
