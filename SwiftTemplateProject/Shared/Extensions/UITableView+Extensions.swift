//
//  NSTableView.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation
import UIKit

//
// MARK: - Register View
extension UITableView {
    /// Helper register cell
    /// The View must conform Identifier protocol
    func registerCell<T: Identifier>(_ viewType: T.Type) {
        self.register(viewType.xib(), forCellReuseIdentifier: viewType.identifierView)
    }
    
    func registerHeaderFooterView<T: Identifier>(_ viewType: T.Type) {
        self.register(viewType.xib(), forHeaderFooterViewReuseIdentifier: viewType.identifierView)
    }

    var lastIndexPath: IndexPath? {
        get {
            let lastSectionIndex = self.numberOfSections - 1
            let lastRowIndex = self.numberOfRows(inSection: lastSectionIndex) - 1
            
            if lastRowIndex >= 0 && lastSectionIndex > 0 {
                return IndexPath.init(row: lastRowIndex, section: lastSectionIndex)
            }
            
            return nil
        }
    }
}
