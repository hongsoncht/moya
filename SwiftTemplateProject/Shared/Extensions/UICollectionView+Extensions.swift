//
//  NSCollectionView+Register.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation
import UIKit

//
// MARK: - Register View
extension UICollectionView {
        
    /// Helper register cell
    /// The View must conform Identifier protocol
    func registerCell<T: Identifier>(_ viewType: T.Type) {
        self.register(viewType.xib(), forCellWithReuseIdentifier: viewType.identifierView)
    }
        
    /// Register Supplementary
    func registerSupplementaryView<T: Identifier>(_ supplementaryType: T.Type, kind: String) {
        self.register(supplementaryType.xib(), forSupplementaryViewOfKind: kind, withReuseIdentifier: supplementaryType.identifierView)
    }
}
