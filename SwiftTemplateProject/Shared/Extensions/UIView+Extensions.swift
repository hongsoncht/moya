//
//  UIView+Extension.swift
//  Aimee
//
//  Created by Pham Hoa on 10/27/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation

extension UIView {
    func getTableViewCell() -> UITableViewCell? {
        
        var superView: UIView? = self.superview
        while superView != nil {
            if superView?.isKind(of: UITableViewCell.self) == true {
                return (superView as! UITableViewCell)
            }
            superView = superView?.superview
        }
        
        return nil
    }
    
    func getCollectionViewCell() -> UICollectionViewCell? {
        
        var superView: UIView? = self.superview
        while superView != nil {
            if superView?.isKind(of: UICollectionViewCell.self) == true {
                return (superView as! UICollectionViewCell)
            }
            superView = superView?.superview
        }
        return nil
    }
}

extension UIView {
    
    var x: CGFloat {
        get {
            return self.frame.origin.x
        }
        set {
            self.frame.origin.x = newValue
        }
    }
    
    var y: CGFloat {
        get {
            return self.frame.origin.y
        }
        set {
            self.frame.origin.y = newValue
        }
    }
    
    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
    
    func removeAllSubviews() {
        let subviews = self.subviews
        for view in subviews {
            view.removeFromSuperview()
        }
    }
}

extension UITableView {
    
    /// Perform a series of method calls that insert, delete, or select rows and sections of the table view.
    /// This is equivalent to a beginUpdates() / endUpdates() sequence,
    /// with a completion closure when the animation is finished.
    /// Parameter update: the update operation to perform on the tableView.
    /// Parameter completion: the completion closure to be executed when the animation is completed.
    
    func performUpdate(_ update: () -> Void, completion: (() -> Void)?) {
        
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        
        // Table View update on row / section
        beginUpdates()
        update()
        endUpdates()
        
        CATransaction.commit()
    }
    
}

extension UIColor {
 func blColorChart () -> [UIColor] {
        return [
            UIColor.init(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0),
            UIColor.init(red: 38/255.0, green: 38/255.0, blue: 38/255.0, alpha: 1.0)
        ]
    }
    
}

extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
}
