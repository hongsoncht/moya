//
//  LocationManager.swift
//  Aimee
//
//  Created by user on 9/28/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit
import CoreLocation

class LocationServiceManager: NSObject, CLLocationManagerDelegate {
    
    // MARK: - Constants
    
    // MARK: - Class Properties
    
    static let shared: LocationServiceManager = LocationServiceManager()

    // MARK: - Instance Properties
    
    // MARK: Stored
    
    private var locationManager: CLLocationManager = CLLocationManager()
    
    // MARK: Computed
    
    // MARK: - Init/Deinit
    
    private override init() {
        //
    }
    
    // MARK: - Instance Methods
    
    // MARK: Private

    // MARK: Public
    
    func requestAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            break
        case .denied:
            AlertManager.sharedInstance.showAlertDefault(nil, message: "Bạn có muốn cho phép ứng dụng truy cập dịch vụ vị trí?", buttons: ["Có", "Không"], completed: { (index) in
                if index == 0 {
                    let path = "App-Prefs:root=Privacy&path=LOCATION"
                    if let settingsURL = URL(string: path), UIApplication.shared.canOpenURL(settingsURL) {
                        if #available(iOS 10, *) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }
            })
            break
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Authorization Status Changed to \(status.rawValue)")
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        default:
            locationManager.stopUpdatingLocation()
        }
    }
    
    func getMyLocation() -> CLLocation? {
        return locationManager.location
    }
}
