//
//  NavigationWithBubbleView.swift
//  Aimee
//
//  Created by Pham Hoa on 3/1/18.
//  Copyright © 2018 Pham Hoa. All rights reserved.
//

import UIKit

class NavigationWithBubbleView: UIView {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var viewBubble: UIView!
    
    class func instanceFromXib() -> NavigationWithBubbleView? {
        return NavigationWithBubbleView.xib()?.instantiate(withOwner: nil, options: nil).first as? NavigationWithBubbleView
    }
}
