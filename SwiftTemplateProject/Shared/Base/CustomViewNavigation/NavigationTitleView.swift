//
//  NavigationTitleView.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit

class NavigationTitleView: UIView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnTitle: UIButton!
    
    class func instanceFromXib() -> NavigationTitleView? {
        return NavigationTitleView.xib()?.instantiate(withOwner: nil, options: nil).first as? NavigationTitleView
    }
}
