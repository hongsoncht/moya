//
//  NavigationBarItemBackView.swift
//  Aimee
//
//  Created by Pham Hoa on 3/15/18.
//  Copyright © 2018 Pham Hoa. All rights reserved.
//

import UIKit

class NavigationBarItemBackView: UIView {
    @IBOutlet weak var lblTtitle: UILabel!
    @IBOutlet weak var btnOverlay: UIButton!
}
