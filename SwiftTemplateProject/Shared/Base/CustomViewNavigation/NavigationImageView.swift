//
//  NavigationImageView.swift
//  Vsign
//
//  Created by Pham Hoa on 3/21/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit

class NavigationImageView: UIView {

    @IBOutlet weak var imgTitle: UIImageView!
    
    class func instanceFromXib() -> NavigationImageView? {
        return NavigationImageView.xib()?.instantiate(withOwner: nil, options: nil).first as? NavigationImageView
    }
}
