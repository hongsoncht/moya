//
//  BaseCell.swift
//  Aimee
//
//  Created by Pham Hoa on 8/30/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit

fileprivate var hasSwizzledUITableViewCell = false
fileprivate var hasSwizzledUITableViewHeaderFooterView = false
fileprivate var hasSwizzledUICollectionReusableView = false

fileprivate var tableViewCellDisposeBagKey: UInt8 = 0
fileprivate var tableViewHeaderFooterViewDisposeBagKey: UInt8 = 0
fileprivate var collectionReusableViewDisposeBagKey: UInt8 = 0

//

extension UITableViewCell {
    var disposeBag: DisposeBag {
        get {
            return associatedObject(base: self, key: &tableViewCellDisposeBagKey, initialiser: {
                return DisposeBag()
            })
        }
        set { associateObject(base: self, key: &tableViewCellDisposeBagKey, value: newValue) }
    }
    
    final public class func doBadSwizzleStuff() {
        guard !hasSwizzledUITableViewCell else { return }
        
        hasSwizzledUITableViewCell = true
        UITableViewCell().swizzPrepareForReuse()
    }
    
    private func swizzPrepareForReuse() {
        let originalSelector = #selector(UITableViewCell.prepareForReuse)
        let swizzledSelector = #selector(UITableViewCell.swizz_prepareForReuse)
        guard let originalMethod = class_getInstanceMethod(UITableViewCell.self, originalSelector) else { return }
        guard let swizzledMethod = class_getInstanceMethod(UITableViewCell.self, swizzledSelector) else { return }
        
        let flag = class_addMethod(UITableViewCell.self, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
        
        if flag {
            class_replaceMethod(UITableViewCell.self, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    @objc dynamic func swizz_prepareForReuse() {
        self.swizz_prepareForReuse()
        disposeBag = DisposeBag()
    }
}

extension UITableViewHeaderFooterView {
    var disposeBag: DisposeBag {
        get {
            return associatedObject(base: self, key: &tableViewHeaderFooterViewDisposeBagKey, initialiser: {
                return DisposeBag()
            })
        }
        set { associateObject(base: self, key: &tableViewHeaderFooterViewDisposeBagKey, value: newValue) }
    }
    
    final public class func doBadSwizzleStuff() {
        guard !hasSwizzledUITableViewHeaderFooterView else { return }
        
        hasSwizzledUITableViewHeaderFooterView = true
        UITableViewHeaderFooterView().swizzPrepareForReuse()
    }
    
    private func swizzPrepareForReuse() {
        let originalSelector = #selector(UITableViewHeaderFooterView.prepareForReuse)
        let swizzledSelector = #selector(UITableViewHeaderFooterView.swizz_prepareForReuse)
        guard let originalMethod = class_getInstanceMethod(UITableViewHeaderFooterView.self, originalSelector) else { return }
        guard let swizzledMethod = class_getInstanceMethod(UITableViewHeaderFooterView.self, swizzledSelector) else { return }
        
        let flag = class_addMethod(UITableViewHeaderFooterView.self, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
        
        if flag {
            class_replaceMethod(UITableViewHeaderFooterView.self, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    @objc dynamic func swizz_prepareForReuse() {
        self.swizz_prepareForReuse()
        disposeBag = DisposeBag()
    }
}

extension UICollectionReusableView {

    var disposeBag: DisposeBag {
        get {
            return associatedObject(base: self, key: &collectionReusableViewDisposeBagKey, initialiser: {
                return DisposeBag()
            })
        }
        set { associateObject(base: self, key: &collectionReusableViewDisposeBagKey, value: newValue) }
    }

    final public class func doBadSwizzleStuff() {
        guard !hasSwizzledUICollectionReusableView else { return }

        hasSwizzledUICollectionReusableView = true
        UICollectionReusableView().swizzPrepareForReuse()
    }

    private func swizzPrepareForReuse() {
        let originalSelector = #selector(UICollectionReusableView.prepareForReuse)
        let swizzledSelector = #selector(UICollectionReusableView.swizz_prepareForReuse)
        guard let originalMethod = class_getInstanceMethod(UICollectionReusableView.self, originalSelector) else { return }
        guard let swizzledMethod = class_getInstanceMethod(UICollectionReusableView.self, swizzledSelector) else { return }

        let flag = class_addMethod(UICollectionViewCell.self, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))

        if flag {
            class_replaceMethod(UICollectionViewCell.self, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }

    @objc dynamic func swizz_prepareForReuse() {
        self.swizz_prepareForReuse()
        disposeBag = DisposeBag()
    }
}
