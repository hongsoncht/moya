//
//  BaseViewController.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import Foundation
import UIKit
import SideMenu
import UIColor_Hex_Swift
import Toast_Swift
import MessageUI

enum ViewControllerType {
    case notHandle
    case none
    case root
    case child
}

enum LeftMenuType {
    case notHandle
    case none
    case menu
    case back
}

enum RightMenuType {
    case notHandle
    case none
    case delete
}

enum NavigationType {
    case none
    case imageOnly(imgName: String)
    case titleOnly(title: String)
    case titleAndDropdown(title: String)
}

extension NSObject {
    func associatedObject<ValueType: AnyObject>(
        base: AnyObject,
        key: UnsafePointer<UInt8>,
        initialiser: () -> ValueType)
        -> ValueType {
            if let associated = objc_getAssociatedObject(base, key)
                as? ValueType { return associated }
            let associated = initialiser()
            objc_setAssociatedObject(base, key, associated,
                                     .OBJC_ASSOCIATION_RETAIN)
            return associated
    }
    
    func associateObject<ValueType: AnyObject>(
        base: AnyObject,
        key: UnsafePointer<UInt8>,
        value: ValueType) {
        objc_setAssociatedObject(base, key, value,
                                 .OBJC_ASSOCIATION_RETAIN)
    }
}

class UIViewControllerSetting {
    var viewControllerType: ViewControllerType = .notHandle
    var leftMenuType: LeftMenuType = .notHandle
    var rightMenuType: RightMenuType = .notHandle
}

class UIViewControllerBag {
    lazy var disposeRxBag: DisposeBag = DisposeBag()
}

private var key: UInt8 = 0 // We still need this boilerplate
extension UIViewController: BaseAbility {
    
    // MARK: - Properties
    
    var setting: UIViewControllerSetting {
        get {
            return associatedObject(base: self, key: &key, initialiser: {
                return UIViewControllerSetting()
            })
        }
        set { associateObject(base: self, key: &key, value: newValue) }
    }

    // MARK: - Conform BaseAbility
    func initCommon() {
        
    }
    
    func configViewController(_ viewControllerType: ViewControllerType, leftType: LeftMenuType, rightType: RightMenuType) {
        self.setting.viewControllerType = viewControllerType
        self.setting.leftMenuType = leftType
        self.setting.rightMenuType = rightType
        setupNavigation()
        setupLeftMenu()
        setupRightMenu()
    }
    
    func setNaivigationTitle(_ type: NavigationType) {
        switch type {
        case .imageOnly(imgName: let name):
            if let viewTitle = NavigationImageView.instanceFromXib() {
                viewTitle.imgTitle.image = UIImage(named: name)
                self.navigationItem.titleView = viewTitle
            }
        case .titleOnly(title: let title):
            let label: UILabel = UILabel.init()
            label.textColor = UIColor.white
            label.textAlignment = NSTextAlignment.center
            label.text = title
            label.sizeToFit()
            self.navigationItem.titleView = label
        case .titleAndDropdown(title: let title):
            if let viewTitle = NavigationTitleView.instanceFromXib() {
                viewTitle.lblTitle.text = title
                self.navigationItem.titleView = viewTitle
            }

        default:
            break
        }
    }
    
    // MARK: - Private
    
    // MARK: Internal
    
    internal func setupNavigation() {
        switch setting.viewControllerType {
        case .notHandle:
            return
        case .none:
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            SideMenuManager.default.menuEnableSwipeGestures = false
        default:
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.view.backgroundColor = UIColor.white
            
            // set default color
            let navigationBarColor = UIColor.init(hexString: Constants.App.navigationBarColor)
            self.setupNavigationBar(color: navigationBarColor)
            
            // add shadow
            self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
            self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            self.navigationController?.navigationBar.layer.shadowRadius = 4.0
            self.navigationController?.navigationBar.layer.shadowOpacity = 1.0
            self.navigationController?.navigationBar.layer.masksToBounds = false
            
            //
            SideMenuManager.default.menuEnableSwipeGestures = true
        }
    }
    
    internal func setupNavigationBar(color: UIColor) {
        var alphaValue: CGFloat = 1.0
        color.getRed(nil, green: nil, blue: nil, alpha: &alphaValue)

        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(color: color), for: .default)
        self.navigationController?.navigationBar.isTranslucent = alphaValue < 1
    }
    
    internal func setupLeftMenu() {
        var image: UIImage?
        switch setting.leftMenuType {
        case .notHandle:
            return
        case .menu:
            image = #imageLiteral(resourceName: "ic_leftmenu")
        case .back:
            image = #imageLiteral(resourceName: "ic_back")
        case .none:
            self.navigationItem.leftBarButtonItem = nil
            return
        }
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: image ?? UIImage(), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.buttonLeftTapped))
        self.navigationItem.leftBarButtonItem = leftButton;
    }
    
    internal func setupRightMenu() {
        var image: UIImage?
        switch setting.rightMenuType {
        case .notHandle:
            return
        case .delete:
            image = #imageLiteral(resourceName: "ic_delete")
        case .none:
            self.navigationItem.rightBarButtonItem = nil
            return
        }
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: image ?? UIImage(), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.buttonRightTapped))
        self.navigationItem.rightBarButtonItem = rightButton;
    }
    
    @objc internal func buttonLeftTapped() {
        switch (setting.leftMenuType) {
        case .menu:
            self.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        case .back:
            _ = self.navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
    
    @objc internal func buttonRightTapped() {
        
    }
}

fileprivate var hasSwizzled = false

extension UIViewController {
    
    final public class func doBadSwizzleStuff() {
        guard !hasSwizzled else { return }
        
        hasSwizzled = true
        UIViewController().swizzViewWillAppear()
    }
    
    private func swizzViewWillAppear() {
        let originalSelector = #selector(UIViewController.viewWillAppear(_:))
        let swizzledSelector = #selector(UIViewController.swizz_viewWillAppear(animated:))
        guard let originalMethod = class_getInstanceMethod(UIViewController.self, originalSelector) else { return }
        guard let swizzledMethod = class_getInstanceMethod(UIViewController.self, swizzledSelector) else { return }
        
        let flag = class_addMethod(UIViewController.self, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
        
        if flag {
            class_replaceMethod(UIViewController.self, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    @objc dynamic func swizz_viewWillAppear(animated: Bool) {
        self.swizz_viewWillAppear(animated: animated)
        print("--SWIZZLED--viewWillAppear : \(self.debugDescription)")
        setupNavigation()
        setupLeftMenu()
        setupRightMenu()
    }
}

func classDomain<T>(_ object: T.Type) -> String {
    return String(describing: object)
}

// MARK: - Base Method
extension UIViewController {
    
    // static methods
    
    static func viewController<T: UIViewController>(from storyboard: String, storyboardID: String? = nil) -> T {
        // swiftlint:disable force_cast
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        if let identifier = storyboardID {
            return storyboard.instantiateViewController(withIdentifier: identifier) as! T
        }
        return storyboard.instantiateViewController(withIdentifier: classDomain(T.self)) as! T
    }
    
    // MARK: Manage Toast
    
    func showToast(message: String) {
        self.view.hideAllToasts()
        self.view.makeToast(message)
    }
    
    //
    func embedInNavigationController() -> UINavigationController {
        return UINavigationController.init(rootViewController: self)
    }
}

extension UIViewController: MFMailComposeViewControllerDelegate {
    func send(to email: String?, content: String?, subject: String?) {
        if !MFMailComposeViewController.canSendMail() {
            self.showToast(message: "general.message.setting.email".localized())
            return
        }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        // Configure the fields of the interface.
        if let email = email {
            composeVC.setToRecipients([email])
        }
        
        composeVC.setSubject(subject ?? "")
        composeVC.setMessageBody(content ?? "", isHTML: false)
        composeVC.mailComposeDelegate = self
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            self.showToast(message: "general.message.setting.email.cancelled".localized())
        case MFMailComposeResult.saved.rawValue:
            self.showToast(message: "general.message.setting.email.saved".localized())
        case MFMailComposeResult.sent.rawValue:
            self.showToast(message: "general.message.setting.email.sent".localized())
        case MFMailComposeResult.failed.rawValue:
            print("general.message.setting.email.sent.failure".localized(), [error!.localizedDescription])
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

extension UIViewController: MFMessageComposeViewControllerDelegate {
    func sendSMS(number: [String]?, content: String?) {
        
        if !MFMailComposeViewController.canSendMail() {
            return
        }
        
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = number
        composeVC.body = content
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    public func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            self.showToast(message: "general.message.setting.sms.cancelled".localized())
        case MFMailComposeResult.saved.rawValue:
            self.showToast(message: "general.message.setting.sms.saved".localized())
        case MFMailComposeResult.sent.rawValue:
            self.showToast(message: "general.message.setting.sms.sent".localized())
        case MFMailComposeResult.failed.rawValue:
            self.showToast(message: "general.message.setting.sms.sent.failure".localized())
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
