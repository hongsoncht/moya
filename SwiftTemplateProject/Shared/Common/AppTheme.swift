//
//  AppTheme.swift
//  Aimee
//
//  Created by Pham Hoa on 1/2/18.
//  Copyright © 2018 Pham Hoa. All rights reserved.
//

import UIKit

class AppTheme: NSObject {
    
    // MARK: - Constants
    
    // MARK: - Setup
    
    static func importFonts() {
        let fonts =
            [Bundle.main.url(forResource: "Renner-Black", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-BlackItalic", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-Bold", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-BoldItalic", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-Book", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-Light", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-LightItalic", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-Medium", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-MediumItalic", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-Thin", withExtension: "otf"),
             Bundle.main.url(forResource: "Renner-ThinItalic", withExtension: "otf")]
        
        fonts
            .compactMap { $0 }
            .compactMap { CGDataProvider(url: $0 as CFURL) }
            .map(CGFont.init)
            .compactMap { $0 }
            .forEach { CTFontManagerRegisterGraphicsFont($0, nil) }
    }
    
    // MARK: - Elements
    
    static func applyBackground(for target: UIView, bgColor: UIColor, cornorRadius: CGFloat = 0) {
        target.backgroundColor = bgColor
        target.layer.cornerRadius = cornorRadius
        target.clipsToBounds = true
    }
    
    // MARK: - Fonts
    
    static func mainBookAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-Book", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainBlackAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-Black", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainBlackItalicFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-BlackItalic", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainBoldAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainBoldItalicAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-BoldItalic", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainLightAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-Light", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainLightItalicAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-LightItalic", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainRegularAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainRegularItalicAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-MediumItalic", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainThinAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-Thin", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func mainThinItalicAppFont(size: CGFloat) -> UIFont {
        return UIFont.init(name: "Renner-it-ThinItalic", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}

class BLColor {
    struct Text {
        /*
        - Using:
             Main text
             Cell label
             Chart figures
         */
        static let primary: UIColor = UIColor.init(rgb: (r: 31, g: 36, b: 46))
        
        /*
         - Using:
             Supporting text
             Placeholder text
         */
        static let secondary: UIColor = UIColor.init(rgb: (r: 115, g: 128, b: 140))
        
        /*
         - Using action:
            Done button
         */
        static let criticalAction: UIColor = UIColor.init(rgb: (r: 0, g: 192, b: 159))
    }
    
    struct Background {
        /* Background Tabs color */
        static let tabItem: UIColor = UIColor.init(rgb: (r: 241, g: 242, b: 244))
        
        /* Background Screen/Search Bar color */
        static let screen: UIColor = UIColor.init(rgb: (r: 227, g: 230, b: 232))
        
        /* Background Button alongside/search bar color*/
        static let btnAlongsideSearchBar: UIColor = UIColor.init(rgb: (r: 213, g: 217, b: 221))
        
        /* Background info Cell color */
        static let infoCell: UIColor = UIColor.init(rgb: (r: 115, g: 128, b: 140))
        
        /* Background icon color */
        static let icon: UIColor = UIColor.init(rgb: (r: 143, g: 153, b: 163))
        
        /* Background button color */
        static let normalButton: UIColor = UIColor.init(rgb: (r: 31, g: 36, b: 46))
        
        /* Background Floating Action Button color */
        static let floatingAction: UIColor = UIColor.init(rgb: (r: 255, g: 102, b: 94))
        
        /* Background Selected cell Color*/
        static let selectedCell: UIColor = UIColor.init(rgb: (r: 118, g: 203, b: 188))
    }
    
    struct Misc {
        /* Line Color */
        static let dividerCell: UIColor = UIColor.init(rgb: (r: 227, g: 230, b: 232))
        
        /* icon Color */
        static let icon: UIColor = UIColor.init(rgb: (r: 143, g: 153, b: 163))
        
        // MARK: ======= Nutrient Level Color ==========
        
        /* Nomal Nutirent level color */
        static let nomalNutrientLevel: UIColor = UIColor.init(rgb: (r: 118, g: 203, b: 188))
        
        /* Low/High Nutrient level color */
        static let lowNutrientlevel: UIColor =  UIColor.init(rgb: (r: 255, g: 205, b: 102))
        static let highNutrientLevel: UIColor =  UIColor.init(rgb: (r: 255, g: 205, b: 102))
        
        /* Deficient, Excessive nutrient level color */
        static let deficientNutrientLevel: UIColor = UIColor.init(rgb: (r: 249, g: 153, b: 134))
        static let excessiveNutrientLevel: UIColor = UIColor.init(rgb: (r: 249, g: 153, b: 134))
        /*===============*/
        
        /* Chart color */
        static let chart: UIColor = UIColor.init(rgb: (r: 182, g: 181, b: 233))
        
    }
}
