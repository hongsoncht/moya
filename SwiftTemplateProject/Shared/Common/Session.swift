//
//  Session.swift
//  Aimee
//
//  Created by Pham Hoa on 1/2/18.
//  Copyright © 2018 Pham Hoa. All rights reserved.
//

import UIKit
import UserNotifications

class Session: NSObject {
    
    // Init
    private override init() {}
    static let shared = Session()

    // MARK: - Properties
    // MARK: Public
    
    var isShowingInvalidSessionAlert = false
    
    // MARK: Private
    private var currentUser: UserProfile?
    
    // MARK: Methods
    
    // Public
    func getCurrentUser() -> UserProfile? {
        if currentUser == nil {
            // get from UserDefaults
            currentUser = getUserFromUserDefaults()
        }
        return currentUser
    }
    
    func saveUserProfile(user: UserProfile?) {
        currentUser = user
        saveUserToUserDefaults(user: user)
    }
    
    func refreshCurrentUserFromLocal() -> UserProfile? {
        currentUser = getUserFromUserDefaults()
        return currentUser
    }
    
    func logout(_ reason: String? = nil) {
        // clear user
        self.clearUserSession()
        
        // back to login screen
        let controller: LoginViewController = LoginViewController.viewController(from: Constants.StoryboardName.main, storyboardID: LoginViewController.identifierView)
        let navController = controller.embedInNavigationController()
        if let window = UIApplication.shared.keyWindow {
            DispatchQueue.main.async {
                UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    window.rootViewController = navController
                }, completion: { _ in
                    if let reason = reason {
                        controller.showToast(message: reason)
                    }
                })
            }
        }
    }
    
    func handleInvalidUserSession() {
        if self.isShowingInvalidSessionAlert {
            return
        }
        
        self.isShowingInvalidSessionAlert = true
        AlertManager.sharedInstance.showAlertDefault(nil, message: "general.message.error.invalid.session".localized(), buttons: ["general.label.ok".localized()]) { (index) in
            self.isShowingInvalidSessionAlert = false
            if index == 0 {
                self.clearUserSession()
            }
        }
    }
    
    func clearUserSession() {
        // clear realm
        RealmManager.shared.clearRealmDB()
        
        // clear user
        self.saveUserProfile(user: nil)
    }
    
    func deleteAccount() {
        //
    }
    
    // Private
    
    private func getUserFromUserDefaults() -> UserProfile? {
        if let data = UserDefaults.standard.object(forKey: "userprofile") as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? UserProfile
        }
        return nil
    }
    
    private func saveUserToUserDefaults(user: UserProfile?) {
        if user != nil {
            let data = NSKeyedArchiver.archivedData(withRootObject: user!)
            UserDefaults.standard.set(data, forKey: "userprofile")
        } else {
            UserDefaults.standard.set(nil, forKey: "userprofile")
        }
        UserDefaults.standard.synchronize()
    }
}
