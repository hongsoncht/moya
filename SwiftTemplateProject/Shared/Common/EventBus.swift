//
//  EventBus.swift
//  Aimee
//
//  Created by user on 10/2/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit

class EventBus: NSObject {
    enum Event: String {
        case logout
        case goToDashboard
        case settingHealthGoalsChanged
        case deleteCellRowInCreatehealthGoal
        case showChangeQuantityVc
        case hideChangingQuantityVc
        case performAddFoodsToMeal
        case settingReminder
        case saveOrUnsaveFood
        case addedFoodsChanged
        case moveToDirectoryFoodScreen
    }
    
    static func post(_ key: Event) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: key.rawValue), object: nil, userInfo: nil)
    }
    
    static func post(_ key: Event, userInfo: [ AnyHashable: Any]) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: key.rawValue), object: nil, userInfo: userInfo)
    }
    
    static func post(forKey key: String) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: key), object: nil, userInfo: nil)
    }
    
    static func post(forKey key: String, userInfo: [ AnyHashable: Any]) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: key), object: nil, userInfo: userInfo)
    }
    
    static func addListener(about: Event, didReceive:@escaping (Notification) -> Void) {
        NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue: about.rawValue),
            object: nil,
            queue: nil,
            using: didReceive)
    }
    
    static func addListener(for key: String, didReceive:@escaping (Notification) -> Void) {
        NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue: key),
            object: nil,
            queue: nil,
            using: didReceive)
    }
}
