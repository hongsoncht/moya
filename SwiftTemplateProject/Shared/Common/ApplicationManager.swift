//
//  ApplicationManager.swift
//  Vsign
//
//  Created by Pham Hoa on 3/20/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit

class ApplicationManager {
    
    // MARK: - Properties
    
    // MARK: Static
    
    static let shared = ApplicationManager()
    
    // MARK: Private
    
    private var config: AppConfig?
    
    //
    // MARK: Public
    
    // MARK: - Methods
    
    // MARK: Public
    
    func getAppConfig() -> AppConfig {
        if config == nil {
            config = getAppConfigToUserDefault()
        }
        
        if config == nil {
            config = AppConfig()
            self.saveAppConfigToUserDefault(config: config)
        }
        
        return config ?? AppConfig()
    }
    
    func saveAppConfig(config: AppConfig?) {
        self.config = config
        self.saveAppConfigToUserDefault(config: self.config)
    }
    
    func cached() {
        self.saveAppConfigToUserDefault(config: self.config)
    }
    
    // MARK: Private
    
    private func saveAppConfigToUserDefault(config: AppConfig?) {
        if config != nil {
            let data = NSKeyedArchiver.archivedData(withRootObject: config!)
            UserDefaults.standard.set(data, forKey: "appConfig")
        } else {
            UserDefaults.standard.set(nil, forKey: "appConfig")
        }
        UserDefaults.standard.synchronize()
    }
    
    private func getAppConfigToUserDefault() -> AppConfig? {
        if let data = UserDefaults.standard.object(forKey: "appConfig") as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? AppConfig
        }
        return nil
    }
}

class AppConfig: EVObject {
    var deviceToken: String?
}
