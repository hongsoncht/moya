//
//  DatePicker.swift
//  Aimee
//
//  Created by Pham Hoa on 10/26/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit
import RMDateSelectionViewController

class DatePicker: NSObject {
    static let sharedInstance = DatePicker()
    
    override private init() {}

    func pickDate(mode: UIDatePicker.Mode = .date, currentDate: Date = Date(), selectedHandler:@escaping ((Date)->()), cancelSelectionHandler: @escaping (()->())) {
        let selectAction: RMAction<UIDatePicker>? = RMAction<UIDatePicker>.init(title: "Select", style: .done) { (pickerController) in
            NSLog("Successfully selected date: \(pickerController.contentView.date)")
            selectedHandler(pickerController.contentView.date)
        }
        
        let cancelAction: RMAction<UIDatePicker>? = RMAction<UIDatePicker>.init(title: "Cancel", style: .cancel) { (_) in
            NSLog("Date selection was canceled")
            cancelSelectionHandler()
        }
        
        if selectAction == nil || cancelAction == nil {
            cancelSelectionHandler()
            return
        }
        
        if let dateSelectionController = RMDateSelectionViewController.init(style: .black, select: selectAction!, andCancel: cancelAction!) {
            dateSelectionController.datePicker.datePickerMode = mode
            dateSelectionController.datePicker.date = currentDate
            if let topVc = UIApplication.shared.keyWindow?.rootViewController {
                topVc.present(dateSelectionController, animated: true, completion: nil)
            } else {
                cancelSelectionHandler()
            }
        } else {
            cancelSelectionHandler()
        }
    }
    
    func pickDate(configure: ((RMDateSelectionViewController) -> Void)?, selectedHandler: ((Date) -> Void)?, cancelSelectionHandler: (() -> Void)?) {
        let selectAction: RMAction<UIDatePicker>? = RMAction<UIDatePicker>.init(title: "Select", style: .done) { (pickerController) in
            NSLog("Successfully selected date: \(pickerController.contentView.date)")
            selectedHandler?(pickerController.contentView.date)
        }
        
        let cancelAction: RMAction<UIDatePicker>? = RMAction<UIDatePicker>.init(title: "Cancel", style: .cancel) { (_) in
            NSLog("Date selection was canceled")
            cancelSelectionHandler?()
        }
        
        if selectAction == nil || cancelAction == nil {
            cancelSelectionHandler?()
            return
        }
        
        if let dateSelectionController = RMDateSelectionViewController.init(style: .black, select: selectAction!, andCancel: cancelAction!) {
            
            configure?(dateSelectionController)
            
            if let topVc = UIApplication.shared.keyWindow?.rootViewController {
                topVc.present(dateSelectionController, animated: true, completion: nil)
            } else {
                cancelSelectionHandler?()
            }
        } else {
            cancelSelectionHandler?()
        }
    }
}
