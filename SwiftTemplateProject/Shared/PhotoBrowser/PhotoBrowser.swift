//
//  PhotoBrowser.swift
//  DatPhuong
//
//  Created by Vuong Le on 10/5/17.
//  Copyright © 2017 Pham Hoa. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class PhotoBrowser: NSObject {

    static let shared: PhotoBrowser = PhotoBrowser.init()
    
    private override init() {
        //
    }
    
    func showPhotoBrowser(images: [UIImage], _ initialIndex: Int = 0) {
        var skImages = [SKPhoto]()
        
        images.forEach({
            let skPhoto = SKPhoto.photoWithImage($0)
            skImages.append(skPhoto)
        })
        
        let browser = SKPhotoBrowser(photos: skImages)
        browser.initializePageIndex(initialIndex)
        Utils.topViewController()?.present(browser, animated: true, completion: nil)
    }
    
    func showPhotoBrowser(urls: [String], _ initialIndex: Int = 0) {
        var skImages = [SKPhoto]()
        
        urls.forEach({
            let validUrl = Constants.App.baseAPIURL + $0
            let photo = SKPhoto.photoWithImageURL(validUrl)
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            skImages.append(photo)
        })
        
        let browser = SKPhotoBrowser(photos: skImages)
        browser.initializePageIndex(initialIndex)
        Utils.topViewController()?.present(browser, animated: true, completion: nil)
    }
    
    func showPhotoBrowser(urlsAndImages: [(String?, UIImage?)], _ initialIndex: Int = 0) {
        var skImages = [SKPhoto]()
        
        urlsAndImages.forEach({
            var photo: SKPhoto?
            if let url = $0.0 {
                let validUrl = Constants.App.baseAPIURL + url
                photo = SKPhoto.photoWithImageURL(validUrl)
            } else if let image = $0.1 {
                photo = SKPhoto.photoWithImage(image)
            }
            photo?.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
            if photo != nil {
                skImages.append(photo!)
            }
        })
        
        let browser = SKPhotoBrowser(photos: skImages)
        browser.initializePageIndex(initialIndex)
        Utils.topViewController()?.present(browser, animated: true, completion: nil)
    }
}
